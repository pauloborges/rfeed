from django.contrib import admin
from rfeed.models import Balanca, Ingrediente, Mesa, Item, IngredientesPorItem, Pedido, ItensPorPedido

admin.site.register(Balanca)
admin.site.register(Ingrediente)
admin.site.register(Mesa)
admin.site.register(Item)
admin.site.register(IngredientesPorItem)
admin.site.register(Pedido)
admin.site.register(ItensPorPedido)