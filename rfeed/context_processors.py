from  django.core.urlresolvers import reverse


def login_page(request):
    return {
        'login_page': request.path == reverse('login')
    }