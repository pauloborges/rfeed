# coding: utf-8
from rfeed.models import Ingrediente
from rfeed.forms import IngredienteForm


class ControladorGerenciarIngredientes(object):
    """"""

    def buscar_ingrediente(self, pk):
        return Ingrediente.objects.get(pk=pk)


    def buscar_ingredientes(self):
        return list(Ingrediente.objects.all())


    def buscar_formulario_ingrediente(self, data=None, pk=None):
        if not pk:
            return IngredienteForm(data=data)

        ingrediente = self.buscar_ingrediente(pk)
        return IngredienteForm(data=data, instance=ingrediente)


    def validar_formulario_ingrediente(self, data, pk=None):
        form = self.buscar_formulario_ingrediente(data, pk)

        if form.is_valid():
            return True, form, None
        else:
            return False, form, form.errors


    def cadastrar_ingrediente(self, nome, balanca):
        return Ingrediente.objects.create(nome=nome, balanca=balanca)


    def atualizar_ingrediente(self, pk, nome, balanca):
        ingrediente = Ingrediente.objects.get(pk=pk)
        ingrediente.nome = nome
        ingrediente.balanca = balanca
        ingrediente.save()
        return ingrediente


    def remover_ingrediente(self, pk):
        try:
            tmp = Ingrediente.objects.get(pk=pk)
            tmp.delete()
            return True
        except:
            return False


    



    