# coding: utf-8
from django.http import QueryDict
from rfeed.models import Pedido, ItensPorPedido
from rfeed.forms import PedidoForm, ItensPorPedidoForm


class ControladorGerenciarPedidos(object):
    """"""

    def buscar_pedido(self, pk):
        return Pedido.objects.get(pk=pk)


    def buscar_pedidos(self):
        return list(Pedido.objects.all())


    def buscar_formulario_pedido(self, data, pk):
        if not pk:
            return PedidoForm(data=data)

        pedido = self.buscar_pedido(pk)
        return PedidoForm(data=data, instance=pedido)


    def validar_formulario_pedido(self, data, pk=None):
        form = self.buscar_formulario_pedido(data=data, pk=pk)
        
        if form.is_valid():
            return True, form, None
        else:
            return False, form, form.errors


    def buscar_formulario_item(self, data=None):
        return ItensPorPedidoForm(data=data)


    def cadastrar_pedido(self, mesa, status):
        return Pedido.objects.create(mesa=mesa, status=status)


    def validar_formularios_itens(self, pedido, data):
        querydicts = [QueryDict('pedido={}&item={}&quantidade={}'.format(pedido, item, quantidade))
                            for item, quantidade in
                            zip(data.getlist(u'item'), data.getlist(u'quantidade'))]

        forms = map(lambda qs: self.buscar_formulario_item(data=qs), querydicts)

        is_valid = all(map(lambda form: form.is_valid(), forms))
        if is_valid:
            return True, forms, None
        else:
            return False, None, {'itens': [f.errors for f in forms]}


    def cadastrar_item(self, pedido, item, quantidade):
        return ItensPorPedido.objects.create(pedido=pedido, item=item, quantidade=quantidade)


    def atualizar_status_pedido(self, pk, status):
        try:
            pedido = self.buscar_pedido(pk)
            pedido.status = status
            pedido.save()
            return True
        except:
            return False


    def remover_pedido(self, pk):
        try:
            tmp = Pedido.objects.get(pk=pk)
            tmp.delete()
            return True
        except:
            return False