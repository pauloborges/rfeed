#coding: utf-8
from django.http import QueryDict
from rfeed.controllers import ControladorGerenciarIngredientes, ControladorGerenciarPedidos

"""
./manage.py dumpdata auth rfeed.balanca rfeed.ingrediente rfeed.item rfeed.ingredientesporitem rfeed.mesa rfeed.pedido rfeed.itensporpedido --format json --indent 4 > rfeed/fixtures/initial_data.json
"""

_instance = None

class Fachada(object):
    """Fachada do sistema RFeed."""

    ingredientes = ControladorGerenciarIngredientes()
    pedidos = ControladorGerenciarPedidos()

    @staticmethod
    def get_instance():
        """Fachada é singleton."""
        global _instance
        if not _instance:
            _instance = Fachada()
        return _instance

    ############################################################
    ####################### INGREDIENTES #######################
    ############################################################

    def buscar_ingrediente(self, pk):
        return self.ingredientes.buscar_ingrediente(pk)


    def buscar_ingredientes(self):
        """Retorna uma lista com todos os Ingredientes."""
        return self.ingredientes.buscar_ingredientes()


    def buscar_formulario_ingrediente(self, data=None, pk=None):
        return self.ingredientes.buscar_formulario_ingrediente(data=data, pk=pk)


    def buscar_formulario_editavel_ingrediente(self, pk):
        if pk:
            form = self.buscar_formulario_ingrediente(pk=pk)
            value = self.buscar_ingrediente(pk).nome
        else:
            form = self.buscar_formulario_ingrediente()
            value = None

        return form, value


    def cadastrar_ingrediente(self, data):
        valid, form, errors = self.ingredientes.validar_formulario_ingrediente(data)

        if valid:
            self.ingredientes.cadastrar_ingrediente(form.cleaned_data['nome'], form.cleaned_data['balanca'])

        return valid, errors


    def atualizar_ingrediente(self, data, pk):
        valid, form, errors = self.ingredientes.validar_formulario_ingrediente(data, pk)

        if valid:
            self.ingredientes.atualizar_ingrediente(pk, form.cleaned_data['nome'], form.cleaned_data['balanca'])

        return valid, errors


    def remover_ingrediente(self, pk):
        return self.ingredientes.remover_ingrediente(pk)


    ############################################################
    ######################### PEDIDOS ##########################
    ############################################################

    def buscar_pedido(self, pk):
        return self.pedidos.buscar_pedido(pk)


    def buscar_pedidos(self):
        def _append_selector(pedido):
            f = self.buscar_formulario_pedido(pk=pedido.pk)
            pedido.status_selector = f['status']

        pedidos = self.pedidos.buscar_pedidos()

        for pedido in pedidos:
            _append_selector(pedido)

        return pedidos


    def buscar_formulario_pedido(self, data=None, pk=None):
        return self.pedidos.buscar_formulario_pedido(data=data, pk=pk)


    def buscar_formulario_editavel_pedido(self, pk=None):
        reset_number()  
        return self.buscar_formulario_pedido(pk=pk)


    def buscar_formulario_item(self, data=None):
        return self.pedidos.buscar_formulario_item(data)


    def buscar_formulario_editavel_item(self):
        return self.buscar_formulario_item(), get_number()


    def atualizar_status_pedido(self, pk, status):
        """"""
        return self.pedidos.atualizar_status_pedido(pk, status)


    def remover_pedido(self, pk):
        return self.pedidos.remover_pedido(pk)


    def cadastrar_pedido(self, data):
        valid, form, errors = self.pedidos.validar_formulario_pedido(data)

        if valid:
            pedido = self.pedidos.cadastrar_pedido(form.cleaned_data['mesa'], form.cleaned_data['status'])

            valid, forms, errors = self.pedidos.validar_formularios_itens(pedido.pk, data=data)
            
            if valid:
                for form in forms:
                    self.pedidos.cadastrar_item(form.cleaned_data['pedido'], form.cleaned_data['item'], form.cleaned_data['quantidade'])
            else:
                self.pedidos.remover_pedido(pedido.pk)

        return valid, errors


__n = 0
def get_number():
    global __n
    __n += 1
    return __n

def reset_number():
    global __n
    __n = 0