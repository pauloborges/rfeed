# coding: utf-8
from django import forms
from django.db.models import Q
from rfeed.models import Ingrediente, Balanca


class IngredienteForm(forms.ModelForm):
    def __init__(self, instance=None, *args, **kwargs):
        super(IngredienteForm, self).__init__(instance=instance, *args, **kwargs)

        if instance:
            self.fields['balanca'].queryset = Balanca.objects.filter(
                 Q(ingrediente=None) | Q(pk=instance.balanca.pk))
            self.fields['balanca'].initial = instance.balanca.pk
        else:
            self.fields['balanca'].queryset = Balanca.objects.filter(ingrediente=None)


    class Meta:
        model = Ingrediente