#encoding: utf-8
from django import forms
from rfeed.models import Pedido, ItensPorPedido


class PedidoForm(forms.ModelForm):
    class Meta:
        model = Pedido
        exclude = ('itens',)


class ItensPorPedidoForm(forms.ModelForm):
    class Meta:
        model = ItensPorPedido

    def __init__(self, instance=None, *args, **kwargs):
        super(ItensPorPedidoForm, self).__init__(instance=instance, *args, **kwargs)

        if instance:
            return
        else:
            self.fields['quantidade'].initial = 1

    def clean_quantidade(self):
        qt = self.cleaned_data['quantidade']

        if qt > 0:
            return qt
        else:
            raise forms.ValidationError('Certifique-se que a quantidade é maior que 1.')