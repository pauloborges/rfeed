# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Balanca'
        db.create_table('balanca', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('ref', self.gf('django.db.models.fields.CharField')(max_length=3)),
            ('peso_atual', self.gf('django.db.models.fields.DecimalField')(default='0.0', max_digits=6, decimal_places=3)),
        ))
        db.send_create_signal('rfeed', ['Balanca'])

        # Adding model 'Ingrediente'
        db.create_table('ingrediente', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('nome', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('balanca', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['rfeed.Balanca'], unique=True)),
        ))
        db.send_create_signal('rfeed', ['Ingrediente'])

        # Adding model 'Mesa'
        db.create_table('mesa', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('codigo', self.gf('django.db.models.fields.CharField')(max_length=3)),
        ))
        db.send_create_signal('rfeed', ['Mesa'])

        # Adding model 'Item'
        db.create_table('item', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('nome', self.gf('django.db.models.fields.CharField')(max_length=100)),
        ))
        db.send_create_signal('rfeed', ['Item'])

        # Adding model 'IngredientesPorItem'
        db.create_table('ingredientes_por_item', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('item', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['rfeed.Item'])),
            ('ingrediente', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['rfeed.Ingrediente'])),
            ('quantidade', self.gf('django.db.models.fields.DecimalField')(max_digits=6, decimal_places=3)),
        ))
        db.send_create_signal('rfeed', ['IngredientesPorItem'])

        # Adding model 'Pedido'
        db.create_table('pedido', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('mesa', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['rfeed.Mesa'])),
            ('status', self.gf('django.db.models.fields.CharField')(default='ABERTO', max_length=6)),
            ('data_hora', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
        ))
        db.send_create_signal('rfeed', ['Pedido'])

        # Adding model 'ItensPorPedido'
        db.create_table('itens_por_pedido', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('pedido', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['rfeed.Pedido'])),
            ('item', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['rfeed.Item'])),
            ('quantidade', self.gf('django.db.models.fields.DecimalField')(max_digits=6, decimal_places=3)),
        ))
        db.send_create_signal('rfeed', ['ItensPorPedido'])


    def backwards(self, orm):
        # Deleting model 'Balanca'
        db.delete_table('balanca')

        # Deleting model 'Ingrediente'
        db.delete_table('ingrediente')

        # Deleting model 'Mesa'
        db.delete_table('mesa')

        # Deleting model 'Item'
        db.delete_table('item')

        # Deleting model 'IngredientesPorItem'
        db.delete_table('ingredientes_por_item')

        # Deleting model 'Pedido'
        db.delete_table('pedido')

        # Deleting model 'ItensPorPedido'
        db.delete_table('itens_por_pedido')


    models = {
        'rfeed.balanca': {
            'Meta': {'object_name': 'Balanca', 'db_table': "'balanca'"},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'peso_atual': ('django.db.models.fields.DecimalField', [], {'default': "'0.0'", 'max_digits': '6', 'decimal_places': '3'}),
            'ref': ('django.db.models.fields.CharField', [], {'max_length': '3'})
        },
        'rfeed.ingrediente': {
            'Meta': {'object_name': 'Ingrediente', 'db_table': "'ingrediente'"},
            'balanca': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['rfeed.Balanca']", 'unique': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nome': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'rfeed.ingredientesporitem': {
            'Meta': {'object_name': 'IngredientesPorItem', 'db_table': "'ingredientes_por_item'"},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'ingrediente': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['rfeed.Ingrediente']"}),
            'item': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['rfeed.Item']"}),
            'quantidade': ('django.db.models.fields.DecimalField', [], {'max_digits': '6', 'decimal_places': '3'})
        },
        'rfeed.item': {
            'Meta': {'object_name': 'Item', 'db_table': "'item'"},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'ingredientes': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['rfeed.Ingrediente']", 'through': "orm['rfeed.IngredientesPorItem']", 'symmetrical': 'False'}),
            'nome': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'rfeed.itensporpedido': {
            'Meta': {'object_name': 'ItensPorPedido', 'db_table': "'itens_por_pedido'"},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'item': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['rfeed.Item']"}),
            'pedido': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['rfeed.Pedido']"}),
            'quantidade': ('django.db.models.fields.DecimalField', [], {'max_digits': '6', 'decimal_places': '3'})
        },
        'rfeed.mesa': {
            'Meta': {'object_name': 'Mesa', 'db_table': "'mesa'"},
            'codigo': ('django.db.models.fields.CharField', [], {'max_length': '3'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'rfeed.pedido': {
            'Meta': {'object_name': 'Pedido', 'db_table': "'pedido'"},
            'data_hora': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'itens': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['rfeed.Item']", 'through': "orm['rfeed.ItensPorPedido']", 'symmetrical': 'False'}),
            'mesa': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['rfeed.Mesa']"}),
            'status': ('django.db.models.fields.CharField', [], {'default': "'ABERTO'", 'max_length': '6'})
        }
    }

    complete_apps = ['rfeed']