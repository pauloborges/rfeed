# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'IngredientesPorItem.unidade'
        db.add_column('ingredientes_por_item', 'unidade',
                      self.gf('django.db.models.fields.CharField')(default='un', max_length=2),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'IngredientesPorItem.unidade'
        db.delete_column('ingredientes_por_item', 'unidade')


    models = {
        'rfeed.balanca': {
            'Meta': {'object_name': 'Balanca', 'db_table': "'balanca'"},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'peso_atual': ('django.db.models.fields.DecimalField', [], {'default': "'0.0'", 'max_digits': '6', 'decimal_places': '3'}),
            'ref': ('django.db.models.fields.CharField', [], {'max_length': '3'})
        },
        'rfeed.ingrediente': {
            'Meta': {'object_name': 'Ingrediente', 'db_table': "'ingrediente'"},
            'balanca': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['rfeed.Balanca']", 'unique': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nome': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'rfeed.ingredientesporitem': {
            'Meta': {'object_name': 'IngredientesPorItem', 'db_table': "'ingredientes_por_item'"},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'ingrediente': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['rfeed.Ingrediente']"}),
            'item': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['rfeed.Item']"}),
            'quantidade': ('django.db.models.fields.DecimalField', [], {'max_digits': '6', 'decimal_places': '3'}),
            'unidade': ('django.db.models.fields.CharField', [], {'default': "'un'", 'max_length': '2'})
        },
        'rfeed.item': {
            'Meta': {'object_name': 'Item', 'db_table': "'item'"},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'ingredientes': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['rfeed.Ingrediente']", 'through': "orm['rfeed.IngredientesPorItem']", 'symmetrical': 'False'}),
            'nome': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'rfeed.itensporpedido': {
            'Meta': {'object_name': 'ItensPorPedido', 'db_table': "'itens_por_pedido'"},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'item': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['rfeed.Item']"}),
            'pedido': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['rfeed.Pedido']"}),
            'quantidade': ('django.db.models.fields.DecimalField', [], {'max_digits': '6', 'decimal_places': '3'})
        },
        'rfeed.mesa': {
            'Meta': {'object_name': 'Mesa', 'db_table': "'mesa'"},
            'codigo': ('django.db.models.fields.CharField', [], {'max_length': '3'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'rfeed.pedido': {
            'Meta': {'object_name': 'Pedido', 'db_table': "'pedido'"},
            'data_hora': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'itens': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['rfeed.Item']", 'through': "orm['rfeed.ItensPorPedido']", 'symmetrical': 'False'}),
            'mesa': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['rfeed.Mesa']"}),
            'status': ('django.db.models.fields.CharField', [], {'default': "'ABERTO'", 'max_length': '6'})
        }
    }

    complete_apps = ['rfeed']