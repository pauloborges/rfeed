from django.db import models
from decimal import Decimal as D

class Balanca(models.Model):
    class Meta:
        app_label = 'rfeed'
        db_table = 'balanca'

    ref = models.CharField("referencia da balanca", max_length=3)
    peso_atual = models.DecimalField("peso atual da balanca", max_digits=6, decimal_places=3, default=D('0.0'))

    def __unicode__(self):
        return u'#{}'.format(self.ref)