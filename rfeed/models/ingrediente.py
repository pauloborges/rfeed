# coding: utf-8
from django.db import models
from rfeed.models import Balanca


class Ingrediente(models.Model):
    class Meta:
        app_label = 'rfeed'
        db_table = 'ingrediente'

    nome = models.CharField("ingrediente", max_length=100)
    balanca = models.OneToOneField(Balanca, verbose_name=u'balança')

    def __unicode__(self):
        return u'{}'.format(self.nome)