from django.db import models
from rfeed.models import Ingrediente


class Item(models.Model):
    class Meta:
        app_label = 'rfeed'
        db_table = 'item'

    nome = models.CharField("nome do item", max_length=100)
    ingredientes = models.ManyToManyField(Ingrediente, through='IngredientesPorItem')

    def __unicode__(self):
        return u'{}'.format(self.nome)


class IngredientesPorItem(models.Model):
    class Meta:
        app_label = 'rfeed'
        db_table = 'ingredientes_por_item'

    UNIDADE_CHOICES = (
        ('Kg', 'Kilogramas'),
        ('g', 'Gramas'),
        ('L', 'Litros'),
        ('mL', 'Mililitros'),
        ('un', 'Unidades')
    )

    item = models.ForeignKey(Item)
    ingrediente = models.ForeignKey(Ingrediente)
    quantidade = models.DecimalField("quantidade", max_digits=6, decimal_places=3)
    unidade = models.CharField("unidade", max_length=2, choices=UNIDADE_CHOICES, default='un')

    def __unicode__(self):
        return u'<IngredientesPorItem item {} ingrediente {} quantidade {}>'.format(
            self.item.nome, self.ingrediente.nome, self.quantidade)