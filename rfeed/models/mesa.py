from django.db import models


class Mesa(models.Model):
    class Meta:
        app_label = 'rfeed'
        db_table = 'mesa'

    codigo = models.CharField("referencia da mesa", max_length=3)

    def __unicode__(self):
        return u'{}'.format(self.codigo)