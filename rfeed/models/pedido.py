from django.db import models
from rfeed.models import Item, Mesa


class Pedido(models.Model):
    class Meta:
        app_label = 'rfeed'
        db_table = 'pedido'

    STATUS_CHOICES = (
        ('Aberto', 'Aberto'),
        ('Fechado', 'Fechado'),
        ('Cancelado', 'Cancelado')
    )

    mesa = models.ForeignKey(Mesa)
    status = models.CharField(max_length=9, choices=STATUS_CHOICES, default='Aberto')
    data_hora = models.DateTimeField("hora/data do pedido", auto_now_add=True)
    itens = models.ManyToManyField(Item, through='ItensPorPedido')

    def __unicode__(self):
        return u'<Pedido: id {} status {} data {} mesa {} num_items {}>'.format(
            self.pk, self.status, self.data_hora, self.mesa.codigo, self.itens.count())


class ItensPorPedido(models.Model):
    class Meta:
        app_label = 'rfeed'
        db_table = 'itens_por_pedido'

    pedido = models.ForeignKey(Pedido)
    item = models.ForeignKey(Item)
    quantidade = models.PositiveSmallIntegerField("quantidade", default=1)

    def __unicode__(self):
        return u'{} {}'.format(self.quantidade, self.item)