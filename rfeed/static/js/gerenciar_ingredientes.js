// Cursor na tabela
$('table > tbody > tr').css('cursor', 'pointer');

var rfeed = {};

rfeed.GerenciarIngredientes = function(action_url) {
    var obj = this;

    this.action_url = action_url;

    $('#button_add_ingrediente').click(function() {
        obj.add_ingrediente_clicked();
    });

    $('table > tbody > tr > .edit').click(function () {
        obj.update_ingrediente_clicked($(this));
    });

    $('.button_rm_ingrediente').click(function() {
        obj.button_rm_ingrediente_clicked($(this));
    });
}


rfeed.GerenciarIngredientes.prototype.add_ingrediente_clicked = function() {
    var obj = this;

    $('#button_modal_confirm').html('Adicionar');
    $('#modal_label').html('Adicionar ingrediente');

    $('#button_modal_confirm').off('click');
    $('#button_modal_confirm').click(function() {
        obj.confirm_add_ingrediente_clicked();
    });

    $.ajax({
        data: '',
        type: 'get',
        url: obj.action_url,
        success: function(response) {
            $('#modal_form_container').html(response);
            $('#modal_container').modal();
        },
        error: function(jqXHR, textStatus, errorThrown){
            console.log(jqXHR.responseText);
        }
    });
}


rfeed.GerenciarIngredientes.prototype.update_ingrediente_clicked = function(button) {
    var obj = this;
    this.ingrediente_update_pk = parseInt($(button).siblings('.pk').html());

    $('#button_modal_confirm').html('Atualizar');
    $('#modal_label').html('Atualizar ingrediente');

    $('#button_modal_confirm').off('click');
    $('#button_modal_confirm').click(function() {
        obj.confirm_update_ingrediente_clicked(button);
    });

    $.ajax({
        data: 'pk=' + obj.ingrediente_update_pk,
        type: 'get',
        url: obj.action_url,
        success: function(response) {
            $('#modal_form_container').html(response);
            $('#modal_container').modal();
        }
    });
}


rfeed.GerenciarIngredientes.prototype.confirm_add_ingrediente_clicked = function() {
    var obj = this;

    $('.modal_errors').html('');

    $.ajax({
        data: $('#modal_form').serialize(),
        type: $('#modal_form').attr('method'),
        url: obj.action_url,
        success: function(response) {
            if (response.success) {
                obj.handle_add_ingrediente_success();
            } else {
                obj.handle_add_ingrediente_error(response.errors);
            }
        }
    });
}


rfeed.GerenciarIngredientes.prototype.handle_add_ingrediente_success = function() {
    $('#modal_container').modal('hide');
    $('.modal_errors').html('');
    location.reload();
}


rfeed.GerenciarIngredientes.prototype.handle_add_ingrediente_error = function(errors) {
    this.handle_ingrediente_form_error(errors);
}


rfeed.GerenciarIngredientes.prototype.button_rm_ingrediente_clicked = function(button) {
    var obj = this;
    var pk = $(button).parent().siblings('.pk').html();
    
    $.ajax({
        data: JSON.stringify({pk: pk}),
        type: 'delete',
        url: obj.action_url,
        success: function(response) {
            if (response.success) {
                obj.handle_rm_ingrediente_success();
            } else {
                obj.handle_rm_ingrediente_error(response.errors);
            }
        }
    });
}


rfeed.GerenciarIngredientes.prototype.handle_rm_ingrediente_success = function() {
    location.reload();
}


rfeed.GerenciarIngredientes.prototype.handle_rm_ingrediente_error = function(errors) {
   //console.log(errors);
}


rfeed.GerenciarIngredientes.prototype.confirm_update_ingrediente_clicked = function(button) {
    var obj = this;
    var pk = $(button).siblings('.pk').html();

    $('.modal_errors').html('');

    $.ajax({
        data: $('#modal_form').serialize() + '&pk=' + pk,
        type: 'put',
        url: obj.action_url,
        success: function(response) {
            if (response.success) {
                obj.handle_update_ingrediente_success();
            } else {
                obj.handle_update_ingrediente_error(response.errors);
            }
        }
    });
}


rfeed.GerenciarIngredientes.prototype.handle_update_ingrediente_success = function() {
    $('#modal_container').modal('hide');
    $('.modal_errors').html('');
    location.reload();
}


rfeed.GerenciarIngredientes.prototype.handle_update_ingrediente_error = function(errors) {
    this.handle_ingrediente_form_error(errors);
}


rfeed.GerenciarIngredientes.prototype.handle_ingrediente_form_error = function(errors) {
    if (errors.nome) {
        $('#error_ingrediente').html(
            '<div class="alert alert-error">' + 
                '<button type="button" class="close" data-dismiss="alert">&times;</button>' + 
                '<span id="modal_errors_span_ingrediente"></span>' +
            '</div>'
        );

        $('#modal_errors_span_ingrediente').html('<strong>Atenção!</strong> ' + errors.nome[0]);
    }

    if (errors.balanca) {
        $('#error_balanca').html(
            '<div class="alert alert-error">' + 
                '<button type="button" class="close" data-dismiss="alert">&times;</button>' + 
                '<span id="modal_errors_span_balanca"></span>' +
            '</div>'
        );

        $('#modal_errors_span_balanca').html('<strong>Atenção!</strong> ' + errors.balanca[0]);
    }
}