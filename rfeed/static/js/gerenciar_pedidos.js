// Cursor na tabela
// $('table > tbody > tr').css('cursor', 'pointer');

var rfeed = {};

rfeed.GerenciarPedidos = function(action_url) {
    var obj = this;

    this.action_url = action_url;

    $('#button_add_pedido').click(function() {
        obj.add_pedido_clicked();
    });

    $('.button_rm_pedido').click(function() {
        obj.button_rm_pedido_clicked($(this));
    });

    // $('table > tbody > tr > .edit').click(function () {
    //     obj.update_pedido_clicked($(this));
    // });

    $('[id=id_status]').addClass('span2');

    $('[id=id_status]').change(function() {
        obj.handle_pedido_status_changed($(this));
    });
}


rfeed.GerenciarPedidos.prototype.show_modal = function(response) {
    var obj = this;

    $('#modal_form_container').html(response);

    $('#modal_container').modal();

    $('#button_add_item').off('click');
    $('#button_add_item').click(function() {
        obj.add_item_clicked();
    });
}


rfeed.GerenciarPedidos.prototype.handle_pedido_status_changed = function(pedido) {
    var obj = this;
    var pk = $(pedido).parent().parent().find('.pk').html();
    var status = $(pedido).find('option:selected').html();

    $.ajax({
        data: 'only_status=true&pk=' + pk + '&status=' + status,
        type: 'put',
        url: obj.action_url,
        success: function(response) {
            if (!response.success)
                location.reload();
        },
        error: function(jqXHR, textStatus, errorThrown){
            console.log(jqXHR.responseText);
        }
    });
}


rfeed.GerenciarPedidos.prototype.add_pedido_clicked = function() {
    var obj = this;

    $('#button_modal_confirm').html('Adicionar');
    $('#modal_label').html('Adicionar pedido');

    $('#button_modal_confirm').off('click');
    $('#button_modal_confirm').click(function() {
        obj.confirm_add_pedido_clicked();
    });

    $.ajax({
        data: '',
        type: 'get',
        url: obj.action_url,
        success: function(response) {
            obj.show_modal(response);
        },
        error: function(jqXHR, textStatus, errorThrown){
            console.log(jqXHR.responseText);
        }
    });
}


rfeed.GerenciarPedidos.prototype.update_pedido_clicked = function(button) {
    var obj = this;
    this.pedido_update_pk = parseInt($(button).parent().find('.pk').html());

    $('#button_modal_confirm').html('Atualizar');
    $('#modal_label').html('Atualizar pedido');

    $('#button_modal_confirm').off('click');
    $('#button_modal_confirm').click(function() {
        obj.confirm_update_pedido_clicked(button);
    });

    $.ajax({
        data: 'pk=' + obj.ingrediente_update_pk,
        type: 'get',
        url: obj.action_url,
        success: function(response) {
            $('#modal_form_container').html(response);
            $('#modal_container').modal();
        },
        error: function(jqXHR, textStatus, errorThrown){
            console.log(jqXHR.responseText);
        }
    });
}

rfeed.GerenciarPedidos.prototype.confirm_add_pedido_clicked = function() {
    var obj = this;

    $('.modal_errors').html('');

    $.ajax({
        data: $('#modal_form').serialize(),
        type: $('#modal_form').attr('method'),
        url: obj.action_url,
        success: function(response) {
            if (response.success) {
                obj.handle_add_pedido_success();
            } else {
                obj.handle_add_pedido_error(response.errors);
            }
        },
        error: function(jqXHR, textStatus, errorThrown){
            console.log(jqXHR.responseText);
        }
    });
}

rfeed.GerenciarPedidos.prototype.handle_add_pedido_success = function() {
    $('#modal_container').modal('hide');
    $('.modal_errors').html('');
    location.reload();
}


rfeed.GerenciarPedidos.prototype.handle_add_pedido_error = function(errors) {
    this.handle_pedido_form_error(errors);
}


rfeed.GerenciarPedidos.prototype.handle_pedido_form_error = function(errors) {
    if (errors.mesa) {
        $('#error_mesa').html(
            '<div class="alert alert-error">' + 
                '<button type="button" class="close" data-dismiss="alert">&times;</button>' + 
                '<span id="modal_errors_span_mesa"></span>' +
            '</div>'
        );

        $('#modal_errors_span_mesa').html('<strong>Atenção!</strong> ' + errors.mesa[0]);
    }

    if (errors.status) {
        $('#error_balanca').html(
            '<div class="alert alert-error">' + 
                '<button type="button" class="close" data-dismiss="alert">&times;</button>' + 
                '<span id="modal_errors_span_status"></span>' +
            '</div>'
        );

        $('#modal_errors_span_status').html('<strong>Atenção!</strong> ' + errors.status[0]);
    }

    if (errors.itens) {
        $('[id=error_item]').each(function(i) {
            var e = errors.itens[i];
            var str = '';

            if (e.quantidade)
                str = e.quantidade[0];
            else if (e.item)
                str = e.item[0];
            else
                str = "Erro desconhecido."

            $(this).html(
                '<div class="alert alert-error">' + 
                    '<button type="button" class="close" data-dismiss="alert">&times;</button>' + 
                    '<span id="modal_errors_span_itens"></span>' +
                '</div>'
            );

            $(this).find('#modal_errors_span_itens').html('<strong>Atenção!</strong> ' + str);
        });
    }
}


rfeed.GerenciarPedidos.prototype.button_rm_pedido_clicked = function(button) {
    var obj = this;
    var pk = $(button).parent().siblings().children('.pk').html();

    console.log(pk);
    
    $.ajax({
        data: JSON.stringify({pk: pk}),
        type: 'delete',
        url: obj.action_url,
        success: function(response) {
            if (response.success) {
                obj.handle_rm_pedido_success();
            } else {
                obj.handle_rm_pedido_error(response.errors);
            }
        },
        error: function(jqXHR, textStatus, errorThrown){
            console.log(jqXHR.responseText);
        }
    });
}


rfeed.GerenciarPedidos.prototype.handle_rm_pedido_success = function() {
    location.reload();
}


rfeed.GerenciarPedidos.prototype.handle_rm_pedido_error = function(errors) {
   //console.log(errors);
}


rfeed.GerenciarPedidos.prototype.add_item_clicked = function() {
    var obj = this;

    $.ajax({
        data: 'item=true',
        type: 'get',
        url: obj.action_url,
        success: function(response) {
            div = $('<div>');
            $(div).append(response);
            $(div).find('#id_item').addClass('span2');

            $(div).find('.button_rm_item').click(function(){
                $(this).parent().parent().parent().remove();
            });

            $('#form_itens').append(div);
            //$('#form_itens').append(response);
            //$('[id=id_item]').addClass('span2')
        },
        error: function(jqXHR, textStatus, errorThrown){
            console.log(jqXHR.responseText);
        }
    });
}