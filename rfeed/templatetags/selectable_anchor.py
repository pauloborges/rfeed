# encoding: utf-8
from django import template
from django.core.urlresolvers import reverse, resolve
import re

register = template.Library()


def remove_quotes(s):
    """Removes bounding quotes from a string if they exist."""
    match = re.match('[\"\'](.*)[\"\']', s, re.UNICODE)
    if match and match.group(1):
        return match.group(1)
    return s


@register.tag
def selectable_anchor(parser, token):
    """
    Parse the selectable anchor template tag. It is mandatory that the
    'request' template context processor is activated.

    This custom tag uses the following syntax:
    {% selectable_anchor reversible_link content_text [class_prefix] %}

    Where:
    `reversible_link` is a string that can be reversible.
    `content_text` is the content of the <a> tag

    When the full-form of the `reversible_link` is equals to the current
    page, this tag will render something like this:

    <a class="active" href="{link}">{content_text}</a>

    But, when the full-form does not match the current page, the rendering will
    be:

    <a class="inactive" href="{link}">{content_text}</a>
    """
    args = token.split_contents()
    if len(args) < 3:
        raise template.TemplateSyntaxError, ('%r tag requires at least two '
            'arguments' % args[0])

    url = remove_quotes(args[1])
    content = remove_quotes(args[2])
    try:
        class_prefix = remove_quotes(args[3])
    except IndexError:
        class_prefix = ''

    return SelectableAnchorNode(url, content, class_prefix)


class SelectableAnchorNode(template.Node):
    def __init__(self, url, content, class_prefix):
        s_url = url.split()
        self.url = reverse(s_url[0], args=s_url[1:])
        self.content = content
        self.param_class = False
        self.param_class_prefix = class_prefix

    def render(self, context):
        request = context['request']
        if request and request.path == self.url:
            self.param_class = True

        if self.param_class:
            return '<li class="active"><a href="%s">%s</a></li>' % (self.url, self.content)

        return '<li><a href="%s">%s</a></li>' % (self.url, self.content)