# coding: utf-8
from django.conf.urls import patterns, include, url
from django.contrib.auth.views import login, logout
from django.core.urlresolvers import reverse_lazy
from rfeed.views import ViewPrincipal, ViewGerenciarPedidos, ViewGerenciarIngredientes

urlpatterns = patterns('',
    url(r'^$', ViewPrincipal.as_view(), name='principal'),
    url(r'^pedidos$', ViewGerenciarPedidos.as_view(), name='pedidos'),
    url(r'^ingredientes$', ViewGerenciarIngredientes.as_view(), name='ingredientes'),

    url(r'^login$', login, {'template_name': 'rfeed/login.html'}, name='login'),
    url(r'^logout$', logout, {'next_page': reverse_lazy('principal')}, name='logout'),
)