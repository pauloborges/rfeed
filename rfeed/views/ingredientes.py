# coding: utf-8
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.views.generic import TemplateView
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse, QueryDict
from rfeed.models import Ingrediente
from rfeed.forms import IngredienteForm
from rfeed.decorators import group_required
from urlparse import parse_qs
import json
from rfeed import Fachada


class ViewGerenciarIngredientes(TemplateView):
    """View que gerencia a adição/remoção/modificação de ingredientes."""

    template_name = 'rfeed/ingredientes.html'
    fachada = Fachada.get_instance()


    @method_decorator(csrf_exempt)
    @method_decorator(group_required('Administrador'))
    def dispatch(self, *args, **kwargs):
        """Despacha a requisição para o método correto: get, post, put ou delete."""
        return super(ViewGerenciarIngredientes, self).dispatch(*args, **kwargs)


    def get_context_data(self, **kwargs):
        """Gera as variáveis para o template da página."""
        context = super(ViewGerenciarIngredientes, self).get_context_data(**kwargs)
        context['ingredientes'] = self.fachada.buscar_ingredientes()

        return context


    def renderizar_formulario_ingrediente(self, request, pk):
        form, value = self.fachada.buscar_formulario_editavel_ingrediente(pk)

        return render_to_response('rfeed/form_ingrediente.html', {'form': form, 'value': value},
            context_instance=RequestContext(request))


    def get(self, request, *args, **kwargs):
        # Pegar formulário para ingrediente
        if request.is_ajax():
            pk = request.GET.get('pk', None)
            return self.renderizar_formulario_ingrediente(request, pk)

        # GET normal
        return super(ViewGerenciarIngredientes, self).get(request, *args, **kwargs)


    def post(self, request):
        valid, errors = self.fachada.cadastrar_ingrediente(request.POST)

        if valid:
            return HttpResponse(json.dumps({'success': True}), content_type='application/json')

        return HttpResponse(json.dumps({'success': False, 'errors': errors}),
            content_type='application/json')


    def put(self, request):
        data = QueryDict(request.body)
        pk = int(data['pk'][0])

        valid, errors = self.fachada.atualizar_ingrediente(data, pk)

        if valid:
            return HttpResponse(json.dumps({'success': True}), content_type='application/json')

        return HttpResponse(json.dumps({'success': False, 'errors': errors}),
            content_type='application/json')


    def delete(self, request):
        data = json.loads(request.body)
        pk = int(data['pk'])
        result = self.fachada.remover_ingrediente(pk)

        return HttpResponse(json.dumps({'success': result}), content_type='application/json')