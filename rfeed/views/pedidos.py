#coding: utf-8
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.views.generic import TemplateView
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse, QueryDict
from rfeed.models import Item, Pedido, ItensPorPedido
from rfeed.decorators import group_required
from urlparse import parse_qs
import json
from rfeed import Fachada


class ViewGerenciarPedidos(TemplateView):
    """View que gerencia a adição/remoção/modificação de pedidos."""

    template_name = 'rfeed/pedidos.html'
    fachada = Fachada.get_instance()

    @method_decorator(csrf_exempt)
    @method_decorator(group_required('Administrador', 'Supervisor'))
    def dispatch(self, *args, **kwargs):
        """Despacha a requisição para o método correto: get, post, put ou delete."""
        return super(ViewGerenciarPedidos, self).dispatch(*args, **kwargs)


    def get_context_data(self, **kwargs):
        """Gera as variáveis para o template da página."""
        context = super(ViewGerenciarPedidos, self).get_context_data(**kwargs)
        context['pedidos'] = self.fachada.buscar_pedidos()

        return context


    def renderizar_formulario_pedido(self, request, pk):
        form = self.fachada.buscar_formulario_editavel_pedido(pk)

        return render_to_response('rfeed/form_pedido.html', {'form': form},
            context_instance=RequestContext(request))


    def renderizar_formulario_item(self, request):
        form, number = self.fachada.buscar_formulario_editavel_item()

        return render_to_response('rfeed/form_pedido_item.html', {'form': form, 'number': number},
            context_instance=RequestContext(request))


    def get(self, request, *args, **kwargs):
        if request.is_ajax():
            if 'item' in request.GET:
                return self.renderizar_formulario_item(request)

            pk = request.GET.get('pk', None)
            return self.renderizar_formulario_pedido(request, pk)

        # GET normal
        return super(ViewGerenciarPedidos, self).get(request, *args, **kwargs)


    def post(self, request):
        valid, errors = self.fachada.cadastrar_pedido(request.POST)

        if valid:
            return HttpResponse(json.dumps({'success': True}), content_type='application/json')

        return HttpResponse(json.dumps({'success': False, 'errors': errors}),
            content_type='application/json')


    def delete(self, request):
        data = json.loads(request.body)
        pk = int(data['pk'])
        result = self.fachada.remover_pedido(pk)

        return HttpResponse(json.dumps({'success': result}), content_type='application/json')


    def put(self, request):
        data = QueryDict(request.body)
        pk = int(data['pk'])

        if 'only_status' in data:
            status = data['status']
            result = self.fachada.atualizar_status_pedido(pk, status)
            return HttpResponse(json.dumps({'success': result}), content_type='application/json')

        return HttpResponse(json.dumps({'success': False}), content_type='application/json')

__n = 0
def get_number():
    global __n
    __n += 1
    return __n

def reset_number():
    global __n
    __n = 0