from django.views.generic import TemplateView


class ViewPrincipal(TemplateView):
    template_name = 'rfeed/principal.html'